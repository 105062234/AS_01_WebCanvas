# Software Studio 2018 Spring Assignment 01 Web Canvas

## My web canvas features
1. **pen**: use this to drow irregular lines
2. **eraser**: use this to clean the lines you draw
3. **line**: use this to drag a line
4. **circle**: use this to drag a circle 
5. **triangle**: use this to drag a triangle
6. **rectangle**: use this to drag a circle
7. **undo/redo**: use this to undi/redo
8. **text**: first you should text some words in the input area,choose size and font. then you can print it on the canvas with mouse clicking.
9. **download**: you can download your picture by clicking the download link
10. **upload**:  you can choose a local image and upload it to the canvas<br>
**(All lines and shapes can choose color and linewidth)**<br>
**(All tools have their own cursor style)**