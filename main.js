window.onload = function (){
    //object
    var canvas = document.querySelector('#SketchPad');
    var colorselect = document.querySelector('#colorselect');
    var ctx = canvas.getContext("2d");
    var pen = document.querySelector('#pen');
    var line = document.querySelector('#line');
    var eraser = document.querySelector('#eraser');
    var circle = document.querySelector('#circle');
    var clear = document.querySelector('#clear');
    var brushsize = document.querySelector('#brushsize');
    var rectangle = document.querySelector('#rectangle');
    var triangle = document.querySelector('#triangle');
    var text = document.querySelector('#text');
    var undo = document.querySelector('#undo');
    var redo = document.querySelector('#redo');
    var upload = document.querySelector('#upload');
    var download = document.querySelector('#download');
    var textinput = document.querySelector('#textinput');
    var textsize = document.querySelector('#textsize');
    var textfont = document.querySelector('#textfont');
   
    //global variables
    var isdraw = false;
    var width = 850;
    var height = 850;
    var DrawType = 0;
    var downX,downY;
    var url = new Array(10000);
    var CurrentIndex = -1;
    var IndexMax = -1;

    //eventlistener
    canvas.addEventListener('mousedown',mousedown);
    canvas.addEventListener('mousemove',mousemove);
    canvas.addEventListener('mouseup',mouseup);
    pen.addEventListener('click',ChangeToPen);
    line.addEventListener('click',ChangeToLine);
    colorselect.addEventListener('change',changecolor);
    eraser.addEventListener('click',ChangeToEraser);
    circle.addEventListener('click',ChangeToCircle);
    clear.addEventListener('click',Clearall);
    rectangle.addEventListener('click',ChangeToRectangle);
    triangle.addEventListener('click',ChangeToTriangle);
    text.addEventListener('click',ChangeToText);
    undo.addEventListener('click',Undo);
    redo.addEventListener('click',Redo);
    upload.addEventListener('change',readfile);
    download.addEventListener('click',function(e){
        download.href = canvas.toDataURL();
    });

    //initialization
    ++CurrentIndex;
    ++IndexMax;
    url[CurrentIndex] = canvas.toDataURL();
    $('#SketchPad').css({'cursor': 'url(pen.cur), auto'});

    function mousedown(e) {
        ctx.lineWidth = brushsize.value;     
        loadcontext();
        if(DrawType==0){
            ctx.beginPath();
            ctx.moveTo(e.pageX-canvas.offsetLeft-30,e.pageY-canvas.offsetTop-50);
        }
        else if(DrawType==1){
            downX=e.pageX-canvas.offsetLeft-30;
            downY=e.pageY-canvas.offsetTop-50;
        }
        else if(DrawType==2){
            ctx.moveTo(e.pageX-canvas.offsetLeft-30,e.pageY-canvas.offsetTop-50);
        }
        else if(DrawType==3){
            downX=e.pageX-canvas.offsetLeft-30;
            downY=e.pageY-canvas.offsetTop-50;
        }
        else if(DrawType==4){
            downX=e.pageX-canvas.offsetLeft-30;
            downY=e.pageY-canvas.offsetTop-50;
        }
        else if(DrawType==5){
            downX=e.pageX-canvas.offsetLeft-30;
            downY=e.pageY-canvas.offsetTop-50;
        }
        else if(DrawType==6){
            ctx.font = textsize.value + "px " + textfont.value;
            ctx.fillText(textinput.value,e.pageX-canvas.offsetLeft-30,e.pageY-canvas.offsetTop-50);
        }
        isdraw = true;
    }

    function mousemove(e) {
        if(DrawType==0){
            if (isdraw) {
                ctx.lineTo(e.pageX-canvas.offsetLeft-30,e.pageY-canvas.offsetTop-50);
                ctx.stroke();
            }
        }
        else if(DrawType==1){
            if (isdraw) {
                ctx.clearRect(0,0,width,height);
                loadcontext();
                ctx.beginPath();
                ctx.moveTo(downX,downY);
                ctx.lineTo(e.pageX-canvas.offsetLeft-30,e.pageY-canvas.offsetTop-50);
                ctx.stroke();
            }
        }
        else if(DrawType==2){
            if (isdraw) {
                ctx.clearRect(e.pageX-canvas.offsetLeft+10-30,e.pageY-canvas.offsetTop+90-50,35,35);
            }
        }
        else if(DrawType==3){
            if(isdraw){
                ctx.clearRect(0,0,width,height);
                loadcontext();
                ctx.beginPath();
                ctx.arc(downX,downY,Math.abs(e.pageX-canvas.offsetLeft-downX-30),0,2*Math.PI,true);
                ctx.fill();
            }
        }
        else if(DrawType==4){
            if(isdraw){
                ctx.clearRect(0,0,width,height);
                loadcontext();
                ctx.beginPath();
                ctx.fillRect(downX,downY,e.pageX-canvas.offsetLeft-downX-30,e.pageY-canvas.offsetTop-downY-50);
                ctx.fill();
            }
        }
        else if(DrawType==5){
            if(isdraw){
                ctx.clearRect(0,0,width,height);
                loadcontext();
                ctx.beginPath();
                ctx.moveTo(downX,downY);
                ctx.lineTo(e.pageX-canvas.offsetLeft-30,e.pageY-canvas.offsetTop-50);
                ctx.lineTo(downX,e.pageY-canvas.offsetTop-50);
                ctx.closePath();
                ctx.fill();
            }
        }
       
    }

    function mouseup(e) {
        isdraw = false;
        if(CurrentIndex >= IndexMax){
            ++CurrentIndex;
            ++IndexMax;
            url[CurrentIndex] = canvas.toDataURL();
        }
        else{
            ++CurrentIndex;
            url[CurrentIndex] = canvas.toDataURL();
        }
    }

    function loadcontext(){
        if(CurrentIndex>=0){
            var img = new Image();
            img.src = url[CurrentIndex];
            ctx.drawImage(img,0,0,width,height);
        }
    }

    function UndoRedoLoad(){
        if(CurrentIndex>=0){
            var img = new Image();
            img.src = url[CurrentIndex];
            img.onload = function(){ctx.drawImage(img,0,0,width,height);}
        }
    }

    function Undo(){
        if(CurrentIndex>0){
            --CurrentIndex;
            ctx.clearRect(0,0,width,height);
            UndoRedoLoad()
        }
    }

    function Redo(){
        if(CurrentIndex<IndexMax){
            ++CurrentIndex;
            ctx.clearRect(0,0,width,height);
            UndoRedoLoad()
        }
    }

    function changecolor(){
        ctx.strokeStyle = colorselect.value;
        ctx.fillStyle = colorselect.value;
    }

    function ChangeToPen(){
        DrawType = 0;
        $('#SketchPad').css({'cursor': 'url(pen.cur), auto'});
    }

    function ChangeToLine(){
        DrawType = 1;
        $('#SketchPad').css({'cursor': 'url(line.cur), auto'});
    }

    function ChangeToEraser(){
        DrawType = 2;
        $('#SketchPad').css({'cursor': 'url(eraser.png), auto'});
    }

    function ChangeToCircle(){
        DrawType = 3;
        $('#SketchPad').css({'cursor': 'url(circle.cur), auto'});
    }

    function ChangeToRectangle(){
        DrawType = 4;
        $('#SketchPad').css({'cursor': 'url(rectangle.cur), auto'});
    }

    function ChangeToTriangle(){
        DrawType = 5;
        $('#SketchPad').css({'cursor': 'url(triangle.cur), auto'});
    }

    function ChangeToText(){
        DrawType = 6;
        $('#SketchPad').css({'cursor': 'url(text.cur), auto'});
    }

    function Clearall(){
        ctx.clearRect(0,0,width,height);
        ++CurrentIndex;
        url[CurrentIndex] = canvas.toDataURL();
    }

    function readfile(){
        var imgfile = upload.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(imgfile);
        reader.addEventListener('load',function(e){
            var img = new Image();
            img.src = this.result;
            img.onload = function(){
                ctx.drawImage(img,0,0,width,height);
                if(CurrentIndex >= IndexMax){
                    ++CurrentIndex;
                    ++IndexMax;
                    url[CurrentIndex] = canvas.toDataURL();
                }
                else{
                    ++CurrentIndex;
                    url[CurrentIndex] = canvas.toDataURL();
                }
            }
        })

    }
}